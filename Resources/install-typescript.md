# Install Typescript

## Global Installation
We need to install typescript and compiler packages using

```bash
sudo npm i -g typescript tsc
```

## Create a new typescript react app
We use the create-react-app too with typescript template
```bash
create-react-app <app-name> --template typescript
```

## Adding typescript to existing project
Install the required missing packages
```bash
npm i --save @types/node @types/react @types/react-dom
```