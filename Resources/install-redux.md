# Install Redux
## Intial one time steps
### Adding reduxt support to the project
Installing the react-redux package

```bash
npm install redux react-redux @reduxjs/toolkit
```
### Defining the store 

By calling the function configure store

### Making store availble for the App

By wrapping the App component with the store provider

## Actions needed for adding a new state

### Define a Slice
We define a slice by calling createSlice
We provide: name, initial, actions (reducers)

## Actions needed for accessing a state
 ### Accessing for read
 use useSelector hook

 ### Accessing for write
 use the hook useDispatch and then dispatch(action)