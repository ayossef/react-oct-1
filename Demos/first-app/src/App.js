import logo from './logo.svg';
import './App.css';
import GameBoard from './components/game/GameBoard';
import EffectHook from './components/hooks/EffectHook';
import ContextHook from './components/hooks/ContextHook';

function App() {
  return (
    <div className="App">
      <header className="App-header">
          {/* <GameBoard></GameBoard> */}
        {/* <EffectHook></EffectHook> */}
        <ContextHook></ContextHook>
      </header>
    </div>
  );
}

export default App;
