import React from 'react'
import { useState } from 'react'

function GuessGame(props) {
    const [userInput, setUserInput] = useState(0)
    const [result, setResult] = useState("")
    const checkValue = () => {
        if(props.num === userInput){
            // Add name and scores
            // score is 10 * remaining trials
            setResult("Congratulations ..")
        }else {

            setResult("Try again")
        }
    }
  return (
    // feeling lucky button should be disabled after 5th trial
    <div>
        <h3>GuessGame</h3>
        <input placeholder='Enter your guess here'
        value={userInput}
        onChange={(event)=>{setUserInput(Number(event.target.value))}}></input>
        <br></br>
        
        <button onClick={checkValue}>Feeling Lucky .. </button>
        <h4>{result}</h4>
    </div>
  )
}

export default GuessGame