import React from 'react'
import GuessGame from './GuessGame'
import { useState } from 'react'

function GameBoard() {
    const [luckyNumber, setLuckyNumber] = useState(0)
    const genNum = () =>{
        setLuckyNumber(Math.round(Math.random()*10))
    }
  return (
    <div>
        <h1>GameBoard</h1>
        <input placeholder='player name'></input>
        <br></br>
        <button onClick={genNum}>New Game</button>
        <GuessGame num = {luckyNumber}></GuessGame>
    </div>
  )
}

export default GameBoard