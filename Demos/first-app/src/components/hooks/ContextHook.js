import React, { createContext, useState } from 'react'
import ContextReader from './ContextReader';

export const scoreContext = createContext();
function ContextHook() {
    const [name, setName] = useState("Player Two")
    const [score, setScore] = useState(0)
    const scoreStore = {
        playerName: name,
        playerScore: score
    }
  return (
    <scoreContext.Provider value={scoreStore}>
        <div>ContextHook
            <p>{scoreStore.playerName}</p>
            <p> Has got score  of {scoreStore.playerScore}</p>
            <ContextReader></ContextReader>
        </div>
    </scoreContext.Provider>
  )
}

export default ContextHook