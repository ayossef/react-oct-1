import React from 'react'
import {scoreContext} from './ContextHook'
import { useContext } from 'react'

function ContextReader() {
    const scoreStore = useContext(scoreContext)
  return (
    <div>ContextReader
        <p>
            Playername shared in context is {scoreStore.playerName}
        </p>
    </div>
  )
}

export default ContextReader