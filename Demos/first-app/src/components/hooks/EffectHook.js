import React, { useState } from 'react'
import { useEffect } from 'react'
function EffectHook() {
    const [counter, setCounter] = useState(0)
    const greetings = ["Hi", "Welcome", "Nice to meet you"]
    const [message, setMessage] = useState("Hello")
    var simpleCounter = 0
    useEffect(() => {
      return () => {
        console.log('Component is Mounted')
      }
    },[])

    useEffect(() => {
      return () => {
        console.log('Message has been changed')
      }
    }, [message])
    

    useEffect(() => {
      return () => {
        console.log('Counter has been updated')
      }
    }, [counter])
    
   
    const updateMessage = () =>{
        
        if(counter === 2) {
            
        }else {
            setCounter(counter + 1)
        }
        setMessage(greetings[counter])
    }
  return (
    <div>
        <h1>Effect Hook</h1>
        <p>{message}</p>
        <button onClick={updateMessage}>New Message</button>
    </div>
  )
}

export default EffectHook