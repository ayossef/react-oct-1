import logo from './logo.svg';
import './App.css';
import Input from './components/Input';
import Display from './components/Display';
import RandomScore from './components/RandomScore';
function App() {
  return (
    <div className="App">
      <header className="App-header">
       <Input></Input>
       <Display></Display>
       <RandomScore></RandomScore>
      </header>
    </div>
  );
}

export default App;
