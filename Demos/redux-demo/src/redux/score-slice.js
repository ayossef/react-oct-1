import { createSlice } from "@reduxjs/toolkit";

export const scoreSlice = createSlice({
    name:'score',
    initialState:{
        value: 0,
        highScore: 0
    },
    reducers:{
        win: (state)=> {state.value = state.value + 1},
        lost: (state) => {state.value = state.value - 1},
        custom:(state, action) => {state.value = state.value + action.payload.cutomInc}
    }
})

export const {win, lost, custom} = scoreSlice.actions
export default scoreSlice.reducer

