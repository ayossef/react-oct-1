import {combineReducers, configureStore} from '@reduxjs/toolkit'
import scoreReducer from './score-slice';

let reducer = combineReducers({
    score: scoreReducer,
})

export default configureStore({
    reducer
})