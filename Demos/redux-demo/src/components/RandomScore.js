import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { custom } from '../redux/score-slice'

function RandomScore() {
    const dispachQ = useDispatch()
    const currentScore = useSelector((state)=>state.score.value)
    const [randScore, setRandScore] = useState(0)
    const randScoreAction = () => {
        setRandScore(Math.round(Math.random()*10) - 5)
    }
    const applyScoreAction = () => {
        
        dispachQ(custom({cutomInc:randScore}))
    }
  return (
    <div>
        <h1>RandomScore</h1>
        <button onClick={randScoreAction}>Get Random Score</button>
        <p>Random score is {randScore} </p>
        <button onClick={applyScoreAction}>Apply new score</button>
        <p>Current Score is {currentScore}</p>
    </div>
  )
}

export default RandomScore