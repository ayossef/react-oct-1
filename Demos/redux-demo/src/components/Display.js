import React from 'react'
import { useSelector } from 'react-redux'

function Display() {
    const scoreValue = useSelector((state)=>state.score.value)
  return (
    <div>
        <h1>Display</h1>
        <p>Value of score from the redux store is</p>
        <p>{scoreValue}</p>
    </div>
  )
}

export default Display