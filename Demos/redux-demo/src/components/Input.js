import React from 'react'
import { useDispatch } from 'react-redux'
import { win } from '../redux/score-slice'
import { lost } from '../redux/score-slice'
function Input() {
    const dispachQ = useDispatch()
    const wonAction = () => {
      dispachQ(win())
    }
    const loseAction = () => {
        dispachQ(lost())
    }
  return (
    <div>
        <h1>Input</h1>
        <button onClick={wonAction}> I Won </button> <br></br>
        <button onClick={loseAction}> I am losing </button>
    </div>
  )
}

export default Input