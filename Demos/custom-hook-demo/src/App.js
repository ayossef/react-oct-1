import logo from './logo.svg';
import './App.css';
import UsersHooked from './components/UsersHooked';
import Posts from './components/Posts';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <UsersHooked></UsersHooked>
      <Posts></Posts>
      </header>
    </div>
  );
}

export default App;
