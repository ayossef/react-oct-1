import { useEffect, useState } from "react"

export const useFetch = (url)=>{
    const [data, setData] = useState([])
    useEffect(() => {
      return () => {
        fetch(url)
        .then((resp)=> resp.json())
        .then((jsonResp)=> setData(jsonResp))
      }
    }, [url])
    return data
}