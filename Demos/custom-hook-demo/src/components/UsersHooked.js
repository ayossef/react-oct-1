import React from 'react'
import { useFetch } from '../hooks/useFetch'

function UsersHooked() {
    const url = "https://jsonplaceholder.typicode.com/users"
    const usersList = useFetch(url)
  return (
    <div>
        <h1>UsersHooked</h1>
        {usersList
        .map(
            (user) => <p>{user.name}</p>
            )
        }
    </div>
  )
}

export default UsersHooked