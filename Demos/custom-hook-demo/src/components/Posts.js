import React from 'react'
import { useFetch } from '../hooks/useFetch'

function Posts() {
    const url = "https://jsonplaceholder.typicode.com/posts"
    const postsList = useFetch(url)
  return (
    <div>
        <h1>Posts</h1>
        {postsList.map((post)=><p>{post.title}</p>)}
    </div>
  )
}

export default Posts