import React, { useEffect, useState } from 'react'

function Users() {
    const url = "https://jsonplaceholder.typicode.com/users"
    const [userList, setuserList] = useState([{name:'User One'}, {name:'User two'}])
    useEffect(() => {
      return () => {
        fetch(url)
        .then((resp)=> resp.json())
        .then((jsonResp)=> setuserList(jsonResp))
      }
    }, [])
    
return (
    <div>
        <h1>Users</h1>
        {userList.map((user)=> <p>{user.email}</p>)}
    </div>

  )
}

export default Users