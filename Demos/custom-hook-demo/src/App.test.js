import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />); // arrange
  const linkElement = screen.getByText(/Posts/i); // act
  expect(linkElement).toBeInTheDocument(); // assert
});


test('should disply 2 as result of 1+1', () => { 
  
  // AAA
  // Arrange
  let num1 = 1
  let num2 = 1

  // Act
  let result = num1 + num2

  // Assert
  expect(result).toBe(2)

 })